
 reverseSentence = (str) => { 
    //  guardas as palavras num array
    let word = []
    // separar string em palavras
    word = str.match(/\S+/g)
    console.log(word)
    // reverter a ordem das palavras
    let result = word.reverse()
    // como está em array, retornar para string
    let converter = result.toString()
    // retirar virgulas e substituir por vazio
    let substituir = converter.replace(/\,/g," ")
    console.log(substituir)
    return substituir
 }
reverseSentence('bob likes dogs')