// exercício 01
// Escreva duas funções de teste unitário chamadas testReverseString1 e testReverseString2.
//  Em seguida, escreva uma função chamada reverseString que inverte uma string.
function testReverseString1() {
    // start testing a happy path
    let result = reverseString("hello") ;
    console.assert(result === "olleh", {
      "function": 'reverseString("hello")',
      "expected": "olleh",
      "result": result
    });
  }
  function testReverseString2() {
    // start testing a happy path
    let result = reverseString1("Howdy") ;
    console.assert(result === "ydwoH", {
      "function": 'reverseString1("Howdy")',
      "expected": "ydwoH",
      "result": result
    });
  }
  
  let reverseString = (string) => {
    //   separar
      let separar = string.split('')
    //   reverter
    let reverter = separar.reverse()
    // juntar
    let juntarArray = reverter.join()
    for (let i =0;i<string.length; i++) {
     juntarArray = juntarArray.replace(',', '')
     console.log(juntarArray)
  }
  return juntarArray
}
reverseString('hello')
testReverseString1()
  
let reverseString1 = (string) => {
  //   separar
    let separar = string.split('')
  //   reverter
  let reverter = separar.reverse()
  // juntar
  let juntarArray = reverter.join()
  for (let i =0;i<string.length; i++) {
   juntarArray = juntarArray.replace(',', '')
   console.log(juntarArray)
}
return null
}
testReverseString2()
// exercício 02
// Escreva duas funções de teste unitário chamadas testReverseSentence1 e testReverseSentence2.
//  Em seguida, escreva uma função chamada reverseSentence que inverte uma frase (ex.: "bob likes dogs" -> "dogs likes bob").
function testReverseSentence1() {
  // start testing a happy path
  let result = reverseSentence("bob likes dogs") ;
  console.assert(result === "dogs likes bob", {
    "function": 'reverseSentence("bob likes dogs")',
    "expected": "dogs likes bob",
    "result": result
  });
}
function testReverseSentence2() {
  // start testing a happy path
  let result = reverseSentence2("lucas likes bitcoin") ;
  console.assert(result === "bitcoin likes lucas", {
    "function": 'reverseSentence("lucas likes bitcoin")',
    "expected": "bitcoin likes lucas",
    "result": result
  });
}
reverseSentence = (str) => { 
  //  guardas as palavras num array
  let word = []
  // separar string em palavras
  word = str.match(/\S+/g)
  console.log(word)
  // reverter a ordem das palavras
  let result = word.reverse()
  // como está em array, retornar para string
  let converter = result.toString()
  // retirar virgulas e substituir por vazio
  let substituir = converter.replace(/\,/g," ")
  // g substitui todas as ocorrẽnciasw
  console.log(substituir)
  return substituir
}
 testReverseSentence1()
 reverseSentence2 = (str) => { 
  //  guardas as palavras num array
  let word = []
  // separar string em palavras
  word = str.match(/\S+/g)
  console.log(word)
  // reverter a ordem das palavras
  let result = word.reverse()
  // como está em array, retornar para string
  let converter = result.toString()
  // retirar virgulas e substituir por vazio
  let substituir = converter.replace(/\,/g," ")
  // g substitui todas as ocorrẽnciasw
  console.log(substituir)
  return null
}
 testReverseSentence2()
// exercício 03
// Escreva duas funções de teste unitário chamadas testMinimumValue1 e testMinimumValue2. 
// Em seguida, escreva uma função chamada minimumValue que encontra o valor mínimo de um array.
function testMinimumValue1() {
  // start testing a happy path
  let result = minimumValue([7,8,1,9,2]) ;
  console.assert(result === 1, {
    "function": 'minimumValue([7,8,1,9,2])',
    "expected": 1,
    "result": result
  });
}
function testMinimumValue2() {
  // start testing a happy path
  let result = minimumValue2([7,8,1,9,2]) ;
  console.assert(result === 1, {
    "function": 'minimumValue2([7,8,1,9,2])',
    "expected": 1,
    "result": result
  });
}
minimumValue = (arr) => {
  //  ordena em ordem crescente
   let ordenar = arr.sort()
  //  pegar o primeiro elemento do array
   console.log(ordenar[0])
   return ordenar[0]
}
testMinimumValue1()
minimumValue2 = (arr) => {
  //  ordena em ordem crescente
   let ordenar = arr.sort()
  //  pegar o primeiro elemento do array
   console.log(ordenar[0])
   return null
}
testMinimumValue2()
// exercício 04
// Escreva duas funções de teste unitário chamadas testMaximumValue1 e testMaximumValue2. Em seguida,
//  escreva uma função chamada maximumValue que encontra o valor máximo de um array.
function testMaximumValue1() {
  // start testing a happy path
  let result = maximumValue([7,8,1,9,2]) ;
  console.assert(result === 9, {
    "function": 'maximumValue([7,8,1,9,2])',
    "expected": 9,
    "result": result
  });
}
function testMaximumValue2() {
  // start testing a happy path
  let result = maximumValue2([7,8,1,9,2]) ;
  console.assert(result === 9, {
    "function": 'minimumValue2([7,8,1,9,2])',
    "expected": 9,
    "result": result
  });
}
maximumValue = (arr) => {
  //  ordena em ordem crescente
   let ordenar = arr.sort()
  //  pegar o primeiro elemento do array
   let reverter = ordenar.reverse()
   console.log(reverter[0])
  //  retornar o maior valor de um array
   return reverter[0]
}
testMaximumValue1()

maximumValue2 = (arr) => {
  //  ordena em ordem crescente
   let ordenar = arr.sort()
  //  pegar o primeiro elemento do array
   let reverter = ordenar.reverse()
   console.log(reverter)
  return null
}
testMaximumValue2()
// Exercício 05
// Escreva duas funções de teste unitário chamadas testCalculateRemainder1 e testCalculateRemainder2.
//  Em seguida, escreva uma função chamada calculateRemainder que calcula o resto (de uma determinada divisão).
function testCalculateRemainder1() {
  // start testing a happy path
  let result = calculateRemainder(4, 2) ;
  console.assert(result === 0, {
    "function": 'calculateRemainder(4, 2)',
    "expected": 0,
    "result": result
  });
}
function testCalculateRemainder2() {
  // start testing a happy path
  let result = calculateRemainder1(4, 2) ;
  console.assert(result === 0, {
    "function": 'calculateRemainder1(4, 2)',
    "expected": 0,
    "result": result
  });
}
calculateRemainder = (a, b) => {
  // calcular o resto de uma determinada divisão
 let resto = a % b;
 console.log(resto)
 return resto
 
}
testCalculateRemainder1()
calculateRemainder1 = (a, b) => {
  // calcular o resto de uma determinada divisão
 let resto = a % b;
 console.log(resto)
 return null
}
testCalculateRemainder2()
// exercício 06
// Escreva duas funções de teste unitário chamadas testDistinctValues1 e testDistinctValues2.
//  Em seguida, escreva uma função chamada distinctValues que retorna valores distintos de uma lista incluindo duplicatas (ou seja, "1 3 5 3 7 3 1 1 5" -> "1 3 5 7").

  function testDistinctValues1() {
    // start testing a happy path
    let result = distinctValues("1 3 5 3 7 3 1 1 5") ;
    console.assert(result === '1 3 5 7', {
      "function": 'distinctValues("1 3 5 3 7 3 1 1 5")',
      "expected": '1 3 5 7',
      "result": result
    });
  }
  function testDistinctValues2() {
    // start testing a happy path
    let result = distinctValues2("1 3 5 3 7 3 1 1 5") ;
    console.assert(result === null, {
      "function": 'distinctValues2("1 3 5 3 7 3 1 1 5")',
      "expected": null,
      "result": result
    });
  }
  function distinctValues(n) {
    let separate = n.split('');
        let find = [];
        for(var i = 0; i < separate.length; i++){
            if(find.indexOf(separate[i]) == -1){
            find.push(separate[i]);
            }
        }
        
        let order = find.sort()
        console.log(order)
        let remove = order.shift()
        console.log(remove)
        let joiner = order.join(' ')
        console.log(joiner)
        return joiner
        
    }
    testDistinctValues1()
    function distinctValues2(n) {
      let separate = n.split('');
          let find = [];
          for(var i = 0; i < separate.length; i++){
              if(find.indexOf(separate[i]) == -1){
              find.push(separate[i]);
              }
          }
          
          let order = find.sort()
          console.log(order)
          let remove = order.shift()
          console.log(remove)
          let joiner = order.join(' ')
          console.log(joiner)
          return joiner
          
      }
      testDistinctValues2()